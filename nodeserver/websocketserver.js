var WebSocketServer = require('ws').Server,
    wss = new WebSocketServer({ port: 8181 });
wss.on('connection', function (ws) {
    console.log('client connected');
    ws.on('message', function (message) {
        var sendData=JSON.stringify([{msg:message}])
        console.log(message.toString());
        wss.clients.forEach(function each(client) {
            client.send(sendData);
        });
        // ws.send("Thank you!"+message);
    });
});