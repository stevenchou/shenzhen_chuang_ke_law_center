var app = getApp();
// socket连接

var util = require('../../utils/util.js');

Page({
  data: {
    type: '',
    title:"",
    token: '',
    url: '',
    bindUrl: ''
  },
  // 页面加载
  onLoad: function (options) {
    // 创建连接
    console.log(options)
    var type = options.type;
    var title = (type == "1"?"BP提交" : "合同审查");
    this.setData({
      type: type,
      title: title
    })

    let that = this


    let token1 = wx.getStorageSync('token')
    var url="";
    if (type=="1") {

      url = util.api + '/gs/company/jump?mobile=' + wx.getStorageSync('isRegistered');

    } else {
     
      url=util.api + '/gs/companycontract/jump?mobile=' + wx.getStorageSync('isRegistered');
     

    }

    that.setData({
      bindUrl: url
    })

  },
  onShow: function (e) {
  },
})


