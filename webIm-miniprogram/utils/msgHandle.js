var webim = require('webim_wx.js');
var webimhandler = require('webim_handler.js');
var fileData = require('userSig.js');


const app = getApp();
function addMsg(msg, currentMsgsArray, callback) {
  var that = this;
  var fromAccount, fromAccountNick, sessType, subType;
  fromAccount = msg.getFromAccount();
  if (!fromAccount) {
    fromAccount = '';
  }
  fromAccountNick = msg.getFromAccountNick();
  if (!fromAccountNick) {
    fromAccountNick = fromAccount;
  }
  //解析消息
  //获取会话类型
  //webim.SESSION_TYPE.GROUP-群聊，
  //webim.SESSION_TYPE.C2C-私聊，
  sessType = msg.getSession().type();
  //获取消息子类型
  //会话类型为群聊时，子类型为：webim.GROUP_MSG_SUB_TYPE
  //会话类型为私聊时，子类型为：webim.C2C_MSG_SUB_TYPE
  subType = msg.getSubType();
  switch (sessType) {
    case webim.SESSION_TYPE.C2C: //私聊消息
      switch (subType) {
        case webim.C2C_MSG_SUB_TYPE.COMMON: //c2c普通消息
          //业务可以根据发送者帐号fromAccount是否为app管理员帐号，来判断c2c消息是否为全员推送消息，还是普通好友消息
          //或者业务在发送全员推送消息时，发送自定义类型(webim.MSG_ELEMENT_TYPE.CUSTOM,即TIMCustomElem)的消息，在里面增加一个字段来标识消息是否为推送消息
          convertMsg(msg, currentMsgsArray, callback); //解析方法
          break;
      }
      break;
  }

};
//end function addMsg
function convertMsg(msg, currentMsgsArray, callback) {
  var that = this;
  var elems, elem, type, content, isSelfSend;
  var loginInfo = app.globalData.userInfo; //自己的资料
  console.log("the loginInfo is:");
  console.log(loginInfo);

  console.log("the msg is:");
  console.log(msg);
  console.log("the fromAccount is " + msg.fromAccount);
  console.log("the identifier is " + app.globalData.identifier);
  if (msg.fromAccount == app.globalData.identifier) {
    console.log("the isSelfSend is true ")
    isSelfSend = true;
  } else {
    console.log("the isSelfSend is false ")
    isSelfSend = false;
  }
  elems = msg.getElems();


  var sess = msg.sess;
  console.log("the sess messages is:");
  console.log(sess.msgs);
  var currentMsg = {}; //设置消息数组，存消息
  var MsgsArray = [];
  MsgsArray =currentMsgsArray;
  for (var i in elems) {
    elem = elems[i];
    type = elem.getType();
    content = elem.getContent();
    switch (type) {
      case webim.MSG_ELEMENT_TYPE.TEXT:
        var msgContent = content;
        var msgTime = msg.getTime(); //得到当前消息发送的时间
        //得到当天凌晨的时间戳
        var timeStamp = new Date(new Date().setHours(0, 0, 0, 0)) / 1000;
        var thisdate;
        var d = new Date(msgTime * 1000); //根据时间戳生成的时间对象
        var min = d.getMinutes();
        var hour = d.getHours();
        //得到时和分，分小于10时，只返回一位数
        if (min < 10) {
          min = "0" + min;
        }
        //得到月份和天  月份一般是从0开始，所以展示出来要+1
        var month = d.getMonth();

        var day = d.getDate();
        //得到时间   当天时间应该只显示时分  当天以前显示日期+时间
        if (timeStamp > msgTime) {
          thisdate = ((month + 1) + '-' + day + ' ' + hour + ":" + min);
        } else {
          thisdate = (hour + ":" + min);
        }
        currentMsg.msgContent = msgContent; //当前消息的内容
        currentMsg.msgTime = thisdate;
        currentMsg.isSelfSend = isSelfSend;
        currentMsg.From_Account = msg.fromAccount;
        console.log("the elem is ")
        console.log(elem);

        //get the headurl
        if (isSelfSend) {
          currentMsg.avatarUrl = app.globalData.FaceUrl;
          console.log("the if currentMsg.avatarUrl is " + currentMsg.avatarUrl);
        }
        else {
          currentMsg.avatarUrl = '../../img/hisconsult.png';
        }
        //然后将每一条聊天消息push进数组
        MsgsArray.push(currentMsg);

        break;
    }
  }
  console.log("the currentMsgsArray is :");
  console.log(currentMsgsArray);
  callback(MsgsArray);
}
//end function convertMsg
function msgHandleInit(username, userSig, companyName, FaceUrl){
  loginInfo.identifier=username;
  loginInfo.identifierNick = companyName;
  loginInfo.headurl=FaceUrl;
  loginInfo.userSig=userSig;
};
var loginInfo = {
  'sdkAppID': app.globalData.sdkAppID, //用户标识接入SDK的应用ID，必填。（这个可以在腾讯云的后台管理看到）
  'appIDAt3rd': app.globalData.sdkAppID, //App 用户使用 OAuth 授权体系分配的 Appid，必填    （这个其实和上面那个是一样的）
  'identifier': '', //用户帐号，必填   （这个就是自己服务器里，每个用户的账号，可以自己设置）
  'identifierNick': '', //用户昵称，选填   (这个填不填都没什么问题，但是我个人觉得，聊天嘛，还是得有一个网名)
  'accountType': app.globalData.accountType, //账号类型，必填   (这个可以在后台管理看到，但是腾讯的文档上是没有这个的！！！但是这个必须填，不填不报错）
  'headurl': '',
  'userSig': '' //鉴权 Token，identifier 不为空时，必填   我觉得这个也是必填的，这个需要在一开始就从后端获取。
}






//
//其他对象，选填
var options = {
  'isAccessFormalEnv': true, //是否访问正式环境，默认访问正式，选填
  'isLogOn': true //是否开启控制台打印日志,默认开启，选填
};


module.exports = {
  addMsg: addMsg,
  convertMsg: convertMsg,
  loginInfo: loginInfo,
  msgHandleInit: msgHandleInit,

  options: options
}