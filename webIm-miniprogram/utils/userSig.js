module.exports = {
  mtData: mtData,
  searchmtdata: searchmtdata
}
var mt_data = mtData()
function searchmtdata(username) {
  var result
  for (let i = 0; i < mt_data.list.length; i++) {
    var mt = mt_data.list[i]
    if (mt.username == username) {
      result = mt
    }
  }
  return result || {}
}

function mtData() {
  var arr = {
    list: [
      {
        id: '1',
        username: 'cs_steven',
        headurl:'../../img/MLC.png',
        userSig: 'eJxlj11PgzAARd-5FaTPxrSUhs1kD0zZh526ORMnL02lhTQolLbgxPjfjWyJJN7Xc3Jv7pfn*z542uwveZbVbeWY*9QS*Fc*gODiD2qtBOOOYSP*QXnUykjGcyfNABEhJIBw7CghK6dydTYyy6yTnaxGihUlG3ZOHSGEKAxxNBkrqhjgXbK7Xi-xS-PY1geRbJZRv6D6fr0lYdq0abNC5StXtitucTylyUes4tr0z87cuPlidaDR1ryVgu7mx7aOyocgzSPeW6wLmlBezGajSafe5fkUniA4xQiNaCeNVXU1CAFEBAUY-gZ4394PjzVfxg__'
      }, {
        id: '2',
        username: 'cssteven',
        headurl: '../../img/MLC.png',
        userSig: 'eJxlj01Pg0AURff8iglrYx4M1NakC0L6YQrRprVFNhM6DPWBwmQYK8X431Vs4iS*7Tm5974PixBib6PNdcZ581Zrps9S2OSW2GBf-UEpMWeZZlTl-6DoJCrBskILNUDH930XwHQwF7XGAi8Gb1stTqI2jDav2FDzG*EBOJ5Hb8amgscBxrN1eLd89vYdqOMjpqPisCjn90m5mkOxkeu4orIJH6ogXfZQ7V8CDHbbvuxkKhYpOHESFefVuxrRAw92CLM64rH7NImyUCV9MJ0alRpfxeUnOvkeNfaoQU9CtdjUg*CC4zsuhZ*zrU-rC*TeXsc_'
      }, {
        id: '3',
        username: 'cs_stevenchou',
        headurl: '../../img/MLC.png',
        userSig: 'eJxlj11vgjAYhe-5FYTbLUtL*dAlu1EhYpA4ncZ507BSWDUrtX2BuWX-fRkzGcnO7fPknJxPy7Zt5ynd3OWM1Y0EChfFHfvedpBz*weVEgXNgRJd-IP8XQnNaV4C1z3Evu*7CA0dUXAJohRXgxlqgLdcste6GWimONF*67fHQwh7HglHQ0VUPVxG22nyOIXJIomDMosvTZXOu3PGXS2X*EiOq3CughviP4NWk3bHuqRKCexDEp3wmkAwluwQidXuvOiiw9qY2cc*G6GXZgMzFG8fBpMg3vj1GBnjwPcCMqAt10bUshdchH3sEvQTx-qyvgEVxmBZ'
      }, {
        id: '4',
        username: 'ck_dfsd',
        headurl: '../../img/consultant.png',
        userSig: 'eJxlj01Pg0AURff9FYS1sTN8FDBxUSkW0jaxtZq6miDzmLxSYWBGoBr-uxGbSOLbnpN77-ucGIZh7teP12mWVe*lZvoswTRuDJOYV39QSuQs1cxu*D8IvcQGWJpraAZIXde1CBk7yKHUmOPFyArGc8VHguIFG1p*ExxCqOPYnj9WUAxwEz2FyTZ8CLo2gFnn1y5UsVctaX7wiOpXItrN5n2Xhh8nWAgVPc8TsbxP2v3xFK8LKTBGXU9fdgsB09VWCg*PQXo43xWb11oR-3ZUqfENLi-ZAfWIQ8eDWmgUVuUgWIS61LLJz5mTr8k3Vstepw__'
      }
    ]
  }
  return arr
} 