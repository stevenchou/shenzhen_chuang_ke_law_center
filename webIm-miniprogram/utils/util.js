function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map    (formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

module.exports = {
  formatTime: formatTime,
    api:"https://www.cdlpkj.cn/school/a",
    // api:"http://192.168.1.139:8080/jeeplus/a",

  wss:'wss:xuezhou.xyz/wss',
    // wss:'ws://192.168.1.139:8080/jeeplus/layIMSocketServer?id=',


    imgurl:'https://www.cdlpkj.cn',
    url:"https://www.cdlpkj.cn",

   
    // wsurl:'192.168.1.100:8080/jeeplus'
}
