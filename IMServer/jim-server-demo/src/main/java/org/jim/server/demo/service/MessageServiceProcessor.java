package org.jim.server.demo.service;

import org.jim.common.ImPacket;
import org.jim.common.packets.ChatBody;
import org.jim.server.command.handler.processor.ProcessorIntf;
import org.jim.server.command.handler.processor.chat.AbstractChatProcessor;
import org.jim.server.command.handler.processor.chat.ChatProcessorIntf;
import org.jim.server.command.handler.processor.chat.DefaultChatProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.ChannelContext;

public class MessageServiceProcessor extends AbstractChatProcessor {
    private Logger logger = LoggerFactory.getLogger(MessageServiceProcessor.class);

    @Override
    public boolean isProtocol(ChannelContext channelContext) {
        return true;
    }

    @Override
    public String name() {
        return "default";
    }

    @Override
    public void handler(ImPacket imPacket, ChannelContext channelContext) throws Exception {
        logger.info("this is MessageServiceProcessor handler");
    }
    @Override
    public void doHandler(ChatBody chatBody, ChannelContext channelContext) {
        logger.info("this is MessageServiceProcessor doHandler");
    }


}
