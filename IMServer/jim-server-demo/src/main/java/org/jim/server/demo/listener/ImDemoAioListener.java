package org.jim.server.demo.listener;

import org.apache.log4j.Logger;
import org.jim.common.ImPacket;
import org.jim.common.ImSessionContext;
import org.jim.common.packets.Command;
import org.jim.common.packets.User;
import org.jim.server.listener.ImServerAioListener;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;

import javax.jws.soap.SOAPBinding;

public class ImDemoAioListener extends ImServerAioListener {
    Logger logger = Logger.getLogger(ImDemoAioListener.class);
    @Override
    public void onAfterSent(ChannelContext channelContext, Packet packet, boolean isSentSuccess) {
        ImPacket imPacket= (ImPacket) packet;
        if (imPacket.getCommand()==Command.COMMAND_LOGIN_RESP||imPacket.getCommand()==Command.COMMAND_HANDSHAKE_RESP){
            ImSessionContext imSessionContext= (ImSessionContext) channelContext.getAttribute();
            User user=imSessionContext.getClient().getUser();

        }
    }

    @Override
    public void onAfterConnected(ChannelContext channelContext, boolean isConnected, boolean isReconnect){
        logger.info("connected");
    }
    @Override
    public void onAfterReceivedBytes(ChannelContext channelContext, int receivedBytes) throws Exception {
        logger.info("onAfterReceivedBytes");
    }

    @Override
    public void onAfterHandled(ChannelContext channelContext, Packet packet, long cost) throws Exception {
        logger.info("onAfterHandled");
    }
}
